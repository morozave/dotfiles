PATH=${PATH}:${HOME}/.bin:${HOME}/.local/bin
source ~/.bin/sourcerer.sh
export VISUAL=vim
export EDITOR=vim
if [ -e .bash_aliases ]; then
	. .bash_aliases
fi
alias ll="ls -l --color"
alias lA="ls -lA --color"

export PS1="\[$(tput bold)\]\[\033[48;5;0m\]\t\[$(tput sgr0)\] \[$(tput bold)\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;40m\]\H\[$(tput sgr0)\]\[\033[38;5;15m\]:\[$(tput bold)\]\w\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[48;5;0m\]\n > \\$ \[$(tput sgr0)\]"
