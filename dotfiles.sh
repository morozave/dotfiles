#!/bin/bash
# options=run install dry
dir="$HOME/.dotfiles"
dir_bkp="$dir"_bkp
bin_dir="$HOME/.bin"
[ -d "$dir" ] && files="$(ls $dir)" #bashrc vimrc tmux.conf Xresources"

usage() { # If no fucks given.
	echo "Usage: `basename $0` [install|run|dry]"
	exit 254
}
[ "$#" -lt "1" ] && usage
#check_connect()
#{ # Check if we have internet connection.
#if [ "$(ping 8.8.8.8 -c1 -w1 -q 2>&1 | awk '$5 ~ /received/ { print $4 }')" == 1 ]; then
#	got_net=1
#else	got_net=0
#fi
#}

#check_pkg()
#{ # For debugging purposes.
#unset installed_packages
#unset need_to_install
#for file in $files; do
#	for P in $(dpkg -S $file | awk '/'$file'/ { print $1 }' | sort | uniq | sed 's/\://g'); do
#		dpkg -l | egrep "$P" | awk '$1 ~ "^ii" { print $2 }' 2>&1 1>/dev/null
#		if 	[ "$P" ]; then installed_packages+=("$P")
#		else	need_to_install+=("P")
#		fi
#	done
#done
#}

check_dirs()
{ # Check if dirs exists. Mark if so.
if [ -d $dir ]; then
	dir_exists=1
else	dir_exists=0
fi
if [ -d $dir_bkp ]; then
	bkp_exists=1
else	bkp_exists=0
fi
}

check_files()
{ # Check if files exists and/or are links. Mark if so.
linked_files=""
for file in $files; do
	if [ -h $HOME/.$file ]; then
		linked_files+=("$file")
	else	real_files+=("$file")
	fi

done
}

#do_install ()
#{ # Install missing packages
#check_connect
#check_pkg
#if [ "${#need_to_install[@]}" -gt "0" ]; then
#	for P in "${need_to_install[@]}"; do
#		if [ "$got_net" == "1" ]; then
#			echo Install package $P
#		else	echo No internet connection.
#		fi
#	done
#else	echo Nothing to do. 
#fi
#}

#do_get ()
#{ # Get dotfiles from hosting.
##echo wget files.
#check_connect
#if [ "$got_net" == "1" ]; then
#        cd ~
#	git pull #cp -ar ~/.dotfiles_orig $dir
#else	echo "No internet connection. :("
#	exit 254
#fi
#}

do_local()
{ # Do local changes. Backup, Make dirs etc
check_dirs
check_files 
if [ "$dir_exists" == "1" ]; then
	[ "$bkp_exists" == "0" ] && mkdir -p $dir_bkp
	if	[ "${#real_files[@]}" -gt "0" ]; then
		for F in ${real_files[@]}; do
			mv $HOME/.$F $dir_bkp/$F
			ln -s $dir/$F $HOME/.$F
		done
	mkdir -p ~/.bin
	mkdir -p ~/.vim/colors 2>/dev/null
	cp ./sourcerer.sh ~/.bin/sourcerer.sh
	cp ./sourcerer.vim ~/.vim/colors/
	fi
else	#do_get
	do_local
fi
}
#do_install
do_local
