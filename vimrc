" Activate pathogen plugin manager
execute pathogen#infect()

" enter the current millenium
set nocompatible

" enable plugins (for netrw)
filetype plugin on

colorscheme sourcerer
" Enable syntax highlighting
syntax on

" Show -- INSERT -- and -- REPLACE -- in status line
set showmode
" Show incomplete commands
set showcmd

" Show liNe numbers to the left
set relativenumber
set number

" Enable mouse support
" set mouse=a

" Show ruler, liNe numbers in status line
set ruler
" Highlight cursor line
set cursorline

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

set hlsearch
set incsearch
" Show non-printable characters
set list
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,nbsp:␣

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")
" ansible-yaml plugin settings
" disable blank line ignore
let g:ansible_options = {'ignore_blank_lines': 0}
" Ctrl+K to search ansible doc
let g:ansible_options = {'documentation_mapping': '<C-K>'}
" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

let g:netrw_winsize = 25
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * :Vexplore
augroup END

